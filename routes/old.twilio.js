Parse = require('parse/node').Parse;

var express = require('express'),
  router = express.Router(),
  twilio = require('../utils/twilio.js'),
  mailgun = require('../utils/mailgun.js');

Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql",
                "c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse";

router.post("/sendText", function(req, res) {
  // Use the Twilio Cloud Module to send an SMS
  return twilio.sendSmsReq({
    From: "+16014531885",
    To: req.body.number,
    Body: req.body.message
  }, req, res);
});

router.post("/sendRentalText", function(req, res){

  console.log("rental text: " + req.body.transactionId);
  // Use the Twilio Cloud Module to send an SMS
  var transId = req.body.transactionId;
  var ownerNumber = req.body.ownerNumber;
  var renterNumber = req.body.renterNumber;
  var requesterId = req.body.requesterId;
  var dressId = req.body.dressId;
  var dressName = req.body.dressName;
  var ownerName = req.body.ownerName;
  var renterName = req.body.renterName;
  var rentalDate = req.body.rentalDate;
  var ownerId = req.body.ownerId;
  
  var messageString = "👗Rental\n" + ownerName + "(+" + ownerNumber + ")➡️\n" + renterName + "(+" + renterNumber + ")\n" + dressName;
 // var messageString = renterName + "(" + requesterId + ", " + renterNumber + ") requests " + dressName + "(" + dressId + ") from " + ownerName + "(" + ownerId + ", " + ownerNumber + ") on " + rentalDate;

  console.log(messageString);

if (req.body.test == "false"){
  twilio.sendSms({
    From: "+16014531885",
    To: "+16016164407",
    Body: messageString
  }); // Eli
    twilio.sendSms({
      From: "+16014531885",
      To: "+19802531715",
      Body: messageString
  }); // William
    twilio.sendSms({
      From: "+16014531885",
      To: "+16013426317",
      Body: messageString
  }); // Sara
    twilio.sendSms({
      From: "+16014531885",
      To: "+17049961324",
      Body: messageString
  }); // David
    twilio.sendSms({
      From: "+16014531885",
      To: "+19802534039",
      Body: messageString
  }); // Clags
  twilio.sendSms({
      From: "+16014531885",
      To: "+16013470228",
      Body: messageString
  });//Brooke

   mailgun.sendEmail({
      to: "hi@curtsyapp.com",
      from: "requests@curtsyapp.com",
      subject: "Dress requested",
      text: messageString
    });
  }
  else{
    console.log("test mode, not sending mass texts");
  }
  return(res.json("good"));
});

router.post("/sendRequestText", function(req, res) {
  // Use the Twilio Cloud Module to send an SMS
//   var transId = req.body.transactionId;
//   var ownerNumber = req.body.ownerNumber;
//   var renterNumber = req.body.renterNumber;
//   var requesterId = req.body.requesterId;
//   var dressId = req.body.dressId;
//   var dressName = req.body.dressName;
//   var ownerName = req.body.ownerName;
//   var renterName = req.body.renterName;
//   var rentalDate = req.body.rentalDate;
//   var ownerId = req.body.ownerId;

//   var messageString = renterName + "(" + requesterId + ", " + renterNumber + ") requests " + dressName + "(" + dressId + ") from " + ownerName + "(" + ownerId + ", " + ownerNumber + ") on " + rentalDate;

// if (req.body.test == false){
//   twilio.sendSms({
//     From: "+16014531885",
//     To: "+16016164407",
//     Body: messageString
//   }); // Eli
//     twilio.sendSms({
//       From: "+16014531885",
//       To: "+19802531715",
//       Body: messageString
//   }); // William
//     twilio.sendSms({
//       From: "+16014531885",
//       To: "+16013426317",
//       Body: messageString
//   }); // Sara
//     twilio.sendSms({
//       From: "+16014531885",
//       To: "+17049961324",
//       Body: messageString
//   }); // David
//     twilio.sendSms({
//       From: "+16014531885",
//       To: "+19802534039",
//       Body: messageString
//   }); // Clags
//   twilio.sendSms({
//       From: "+16014531885",
//       To: "+16013470228",
//       Body: messageString
//   });//Brooke

//    mailgun.sendEmail({
//       to: "hi@curtsyapp.com",
//       from: "requests@curtsyapp.com",
//       subject: "Dress requested",
//       text: messageString
//     });
//   }
//   else{
//     console.log("test mode, not sending mass texts");
//   }
});

router.post("/sendCancellationText", function(req, res) {
  // Use the Twilio Cloud Module to send an SMS

  var ownerCancel = req.body.ownerCancel;
  var transId = req.body.transactionId;
  var ownerNumber = req.body.ownerNumber;
  var renterNumber = req.body.renterNumber;
  var dressId = req.body.dressId;
  var dressName = req.body.dressName;
  var ownerName = req.body.ownerName;
  var renterName = req.body.renterName;

  var messageString = ""
  var curtsyMessage = ""
  var recipient = ""
  console.log("canceling rental " + transId);
  if (ownerCancel == true){
    //send text to renter number
    messageString = ownerName + " has cancelled your rental for " + dressName + ". If you have any questions, reply to this message to talk to Curtsy support."
    curtsyMessage = ownerName + " (phone: " + ownerNumber + ") cancelled " + renterName + "'s (phone:" + renterNumber + ") rental for " + dressName
    recipient = renterNumber;
  }
  else{
    //send text to owner number
    messageString = renterName + " has cancelled her rental for your " + dressName + ". If you have any questions, reply to this message to talk to Curtsy support."
    curtsyMessage = renterName + " (phone: " + ownerNumber + ") cancelled her rental for " + renterName + "'s (phone:" + renterNumber + ") " + dressName
    recipient = ownerNumber
 }

if (req.body.test == false){
  twilio.sendSms({
    From: "+16014531885",
    To: "+16016164407",
    Body: curtsyMessage
  }); // Eli
    twilio.sendSms({
      From: "+16014531885",
      To: "+19802531715",
      Body: curtsyMessage
  }); // William
    twilio.sendSms({
      From: "+16014531885",
      To: "+17049961324",
      Body: curtsyMessage
  }); // David
    twilio.sendSms({
      From: "+16014531885",
      To: "+19802534039",
      Body: curtsyMessage
  }); // Clags
  twilio.sendSms({
      From: "+16014531885",
      To: "+16013470228",
      Body: curtsyMessage
  });//Brooke

  }
  else{
    console.log("test mode, not sending mass texts");
  }

  //send to requester
  return twilio.sendSmsReq({
    From: "+16014531885",
    To: recipient,
    Body: messageString
  }, req, res);
});

router.get("/test", function(req, res) {
  return res.json("hello");
});

module.exports = router;
