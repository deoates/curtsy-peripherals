var express = require('express'),
  router = express.Router(),
  mailgun = require('../utils/mailgun.js'),
  sg = require('sendgrid')('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'),
  request = require('request'),
  curtsyEmailApi = "http://104.236.195.253:3000";

router.post("/reportEmail", function(req, res) {
  return mailgun.sendEmailReq({
    to: "eeallen1@go.olemiss.edu",
    from: "CurtsyReport@curtsyapp.com",
    subject: "A dress has been reported",
    text: "Check out dress " + req.body.title + " id: " + req.body.objectID + ", user: " + req.body.name
  }, req, res);
});

router.get("/isAlive", function(req, res){
  return res.json({
      success: true,
      message: "success"
    });
});

router.get("/sgtest", function(req, res) {
  var helper = require('sendgrid').mail
  from_email = new helper.Email('hi@curtsy.club');
  to_email = new helper.Email('eeallen1@go.olemiss.edu');
  subject = 'Hi, testing our email service';
  content = new helper.Content('text/plain', "You can ignore this :)");
  mail = new helper.Mail(from_email, subject, to_email, content);

  var request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON()
  });

  sg.API(request, function(error, response) {
    console.log(response.statusCode);
    console.log(response.body);
    console.log(response.headers);
  });

  res.send("hello");
  return res.json({
        success: true,
        message: "Email sent!"
      });
});

router.post("/loginEmail", function(req, res) {
  var email = req.body.email.toLowerCase();
  console.log("login for " + email + "\ncode: " + req.body.confirmation);
  var url = curtsyEmailApi + "/verify/" + email + "/code/" + req.body.confirmation;
  request(url, function(error, res, body) {
    if (error) return console.log(error);
    if (body) return console.log(body);
  });
  // if((email.indexOf('@live.unc.edu') >= 0) || (email.indexOf("uky.edu") >= 0) || (email.indexOf("auburn.edu") >= 0) || (email.indexOf("curtsyapp.com") >= 0)){
  //     return mailgun.sendEmailReq({
  //       to: req.body.email,
  //       bcc: "hi@curtsy.club",
  //       from: "Team Curtsy <hi@curtsy.club>",
  //       subject: "Your Curtsy code is " + req.body.confirmation,
  //       text: "Your Curtsy confirmation code is " + req.body.confirmation + ".\n We hope you enjoy using Curtsy!"
  //     }, req, res);
  // }
  // else{
  //   var helper = require('sendgrid').mail
  //   from_email = new helper.Email("hi@curtsy.club", "Team Curtsy");
  //   to_email = new helper.Email(email);
  //  // personalization = new helper.Personalization()
  //   //email = new helper.Email("hi@curtsyapp.com", "Team Curtsy");
  //   // personalization.addTo(to_email);
  //   // personalization.addCc(email);
  //   subject = 'Your Curtsy code is ' + req.body.confirmation;
  //   content = new helper.Content('text/plain', "Your Curtsy confirmation code is " + req.body.confirmation + ".\n We hope you enjoy using Curtsy!");
  //   mail = new helper.Mail(from_email, subject, to_email, content);
  //   //mail.addPersonalization(personalization)
  //   var request = sg.emptyRequest({
  //     method: 'POST',
  //     path: '/v3/mail/send',
  //     body: mail.toJSON()
  //   });
  //   sg.API(request, function(error, response) {
  //     console.log(response.statusCode);
  //     console.log(response.body);
  //     console.log(response.headers);
  //   });
  // //res.send("hello");
  //   return res.json({
  //       success: true,
  //       message: "Email sent!"
  //   });
  // }
});

router.post("/loginSendGrid", function(req, resp){
  var email = req.body.email.toLowerCase();
	email = vcEmailHackCheck(email);
  var url = curtsyEmailApi + "/verify/" + email + "/code/" + req.body.confirmation + "?provider=sendgrid";
  if((email.indexOf('@live.unc.edu') >= 0) || (email.indexOf("uky.edu") >= 0) || (email.indexOf("auburn.edu") >= 0)){
    //if it's these emails, don't even use sendgrid
    url = curtsyEmailApi + "/verify/" + email + "/code/" + req.body.confirmation + "?provider=mailgun";
  }  
  console.log("SendGrid login for " + email + "\ncode: " + req.body.confirmation);
  request(url, function(error, res, body) {
    console.log("error: " + error);
    console.log("body: " + body)
    if (error) return resp.json({
      success: false,
      message: "error"
    });
    if (body) return resp.json({
      success: true,
      message: "success"
    });
  });
  // var helper = require('sendgrid').mail
  //   from_email = new helper.Email("hi@curtsy.club", "Team Curtsy");
  //   to_email = new helper.Email(email);
  //   subject = 'Your Curtsy code is ' + req.body.confirmation;
  //   content = new helper.Content('text/plain', "Your Curtsy confirmation code is " + req.body.confirmation + ".\n We hope you enjoy using Curtsy!");
  //   mail = new helper.Mail(from_email, subject, to_email, content);
  //   var request = sg.emptyRequest({
  //     method: 'POST',
  //     path: '/v3/mail/send',
  //     body: mail.toJSON()
  //   });
  //   sg.API(request, function(error, response) {
  //     return res.json({
  //       success: error
  //     });
  //   });
});

function vcEmailHackCheck(email) {
  if (email.split("@").length > 2) {
		if (email.indexOf("@curtsyapp.com") > -1) {
			return email.replace("@curtsyapp.com", "");
		}
	}
	return email;
}

router.post("/loginMailgun", function(req, resp){
  var email = req.body.email.toLowerCase();
	email = vcEmailHackCheck(email);
  console.log("Mailgun login for " + email + "\ncode: " + req.body.confirmation);
  var url = curtsyEmailApi + "/verify/" + email + "/code/" + req.body.confirmation + "?provider=mailgun";
  request(url, function(error, res, body) {
    console.log("error: " + error);
    console.log("body: " + body)
    if (error) return resp.json({
      success: false,
      message: "error"
    });
    if (body) return resp.json({
      success: true,
      message: "success"
    });
  });
});


module.exports = router;
