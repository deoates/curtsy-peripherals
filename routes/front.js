var express = require('express'),
  Parse = require('parse/node').Parse,
  router = express.Router();

Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql",
                "c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse"

router.post("/reply", function(req, res) {


  var conversation = req.body._links.related.conversation;
  var tokens = conversation.split('/');
  var convoId = tokens[tokens.length - 1];

  var email = req.body.recipients[1].handle;
  var text = req.body.text.replace(/^\s+|\s+$/g, '');
  console.log("Conversation: " + convoId);
  console.log("Email: " + email);

  var query = new Parse.Query(Parse.User);
  query.equalTo("email", email);
  query.first({
    success:function(user){
      console.log("found user");
      var conversation = user.get('supportConversation');
      console.log(conversation);
      conversation.set("frontId", convoId);
      var supportQuery = new Parse.Query(Parse.User);
      var Message = Parse.Object.extend("Message");
      var message = new Message();
      console.log("got user");
      message.set("recipient", user);
      message.set("recipientId", user.id);
      message.set("conversationId", conversation.id);
      message.set("text", text);
      supportQuery.equalTo("support", true);
      supportQuery.first({
        success:function(supporter){
          console.log("got supporter");
          message.set("sender", supporter);
          message.set("senderId", supporter.id);
          message.save(null,{
            success: function(mess){
              console.log("message saved");
              var messages = conversation.relation("messages");
              messages.add(mess);
              conversation.set("unreadRecipients", [user.id]);
              conversation.set("lastMessage", text);
              conversation.set("lastMessageDate", new Date());
              conversation.save();
              Parse.Cloud.run("addUserToConversation", {userId:user.id, conversationId:conversation.id});
              Parse.Cloud.run("sendPush", {text: text, userId: user.id, senderName: supporter.get("firstName")});
              return res.json(mess);
            }, error: function(error){
              console.log("message error: " + error);
              return res.json(error);
            }
          });
        }, error: function(error){
          console.log("query error: " + error);
          return res.json(error);
        }
      });
    }, error: function(error){
      console.log(error);
      return res.json(error);
    }
  });
  // var conversation = req.body._links.related.conversation;
  // var tokens = conversation.split('/');
  // var convoId = tokens[tokens.length - 1];
  // var email = req.body.recipients[1].handle;
  // var text = req.body.text;

  // console.log(conversation);
  // console.log(convoId);
  // console.log(email);
  // console.log(text);

  // Parse.Cloud.run("replyFromFront", {conversation: conversation, convoId: convoId, email: email, text: text}, {
  //   success: function(result){
  //     console.log(result);
  //     res.status(200);
  //     return res.json(result);
  //   },
  //   error: function(error){
  //     console.log(error);
  //     res.status(200);
  //     return res.json(error);
  //   }
  // });
});



module.exports = router;