var ColorThief = require('color-thief'),
    Parse = require('parse/node').Parse,
    express = require('express'),
    router = express.Router();

Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql",
  "c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse";

router.post("/getColors", function(req, res){
	var dressId = req.body.dressId;
	var query = new Parse.Query("Dress");
	query.equalTo("objectId", dressId);
	query.first({
		success: function(dress){
			if(!dress){
				console.log("unable to find dress with Id: " + dressId);
				return res.json({
					success: false,
					message: "Dress does not exist"
				});
			}
			else{
				console.log("Coloring images for dress " + dress.get("title"));
				var images = dress.relation("images");
				var imageQuery = images.query();
				imageQuery.find({
					success: function(images){
						getColors(images, function(){
							return res.json({
								success: true,
								message: "Images colored for dress"
							});
						});
					},
					error: function(error){
						console.log(error);
						return res.json({
							success: false,
							message: error;
						});
					}
				});
			}
		},
		error: function(error){
			console.log(error);
			return res.json({
				success: false,
				message: error;
			});
		}
	});
});

function getColors(images, callback){
	console.log("finding colors for " + images.length + " images");
	var i = 0;
	function color(){
		if(i >= images.length) return callback();
		colorImage(images[i], function(){
			i++
			color();
		});
	}
	color();
}

function colorImage(image, callback){
	image.fetch().then(function(){
		var color = colorThief.getColor(image.get("file"));
		if(color){
			console.log("found color " + color + " for image " + image.id);
			image.set("color", color);
			image.save().then(function(){
				callback();
			});
		}
	});
}



