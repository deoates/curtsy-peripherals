Parse = require('parse/node').Parse;

var express = require('express'),
    router = express.Router(),
    twilio = require('../utils/twilio.js'),
    mailgun = require('../utils/mailgun.js'),
    Slack = require('slack-node'),
    moment = require("moment");

Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql",
  "c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse";

// Slack config
var webhookUri = "https://hooks.slack.com/services/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";
var slack = new Slack();
slack.setWebhook(webhookUri);

router.post("/sendText", function(req, res) {
	// Use the Twilio Cloud Module to send an SMS
	console.log("/sendText \nsending message: " + req.body.message + " to " + req.body.number);
	return twilio.sendSmsReq({
		From: "+16014531885",
		To: req.body.number,
		Body: req.body.message
	}, req, res);
});


function getOrdinal(n) {
   var s=["th","st","nd","rd"],
       v=n%100;
   return n+(s[(v-20)%10]||s[v]||s[0]);
}

// for debugging
function queryRentals() {
    var Transaction = Parse.Object.extend("Transaction"),
        query = new Parse.Query(Transaction);

    query.include("owner");
    query.include("owner.school");
    query.include("dress");
    query.include("renter");
    query.include("paymentMethod");
    query.descending("createdAt");
    query.limit(100);

    query.find().then(function(rentals) {
        var i = 0;
        function process() {
            console.log(i);
            if (i >= rentals.length) return console.log("done!");
            var rental = rentals[i];
            postRentalInSlack(rental.id, function() {
                i++;
                process();
            });
        }
        process();
    });

}

function postRentalInSlack(id) {
    
    var Transaction = Parse.Object.extend("Transaction"),
        query = new Parse.Query(Transaction);

    query.equalTo("objectId", id);

    query.include("owner");
    query.include("owner.school");
    query.include("dress");
    query.include("renter");
    query.include("paymentMethod");

    query.first().then(function(rental) {
        var owner = rental.get("owner"),
            renter = rental.get("renter"),
            dress = rental.get("dress");

        if (!dress || !owner || !renter) return callback();

        var paymentType = function() {
                if (rental.get("paymentMethod")) {
                    return rental.get("paymentMethod").get("type");
                }
                return "unknown";
            }(),
            managed = dress.get("managed"),
            dressName = dress.get("title"),
            ownerName = owner.get("fullName"),
            schoolName = function() {
                if (owner && owner.get("school")) {
                    return owner.get("school").get("colloquialName");
                }
            }(),
            subtotal = rental.get("originalPrice"),
            total = rental.get("finalPrice"),
            convoId = function() {
                if (rental.get("conversation")) {
                    return rental.get("conversation").id;
                }
            }(),
            tryOn = rental.get("tryOn"),
            renterName = renter.get("fullName"),
            renterFirstName = renter.get("firstName"),
            modApi = "http://mod.curtsyapp.com/dresses",
            convoApi = "http://104.131.135.215:3000/conversation/" + convoId,
            verb = "rented",
            text = "";

        if (tryOn) {
            verb = "is trying on";
        }

        if (managed) {
            text += "Trunk Show Rental\n";
        }

        // David Oates rented Name of Dress
        text += "<" + modApi + "?u=" + renter.id + "|" + renterName + "> " +
                verb + " <" + modApi +"?id=" + dress.id + "|" + dressName + ">\n";

        text += "From " + "<" + modApi + "?u=" + owner.id + "|" +
                ownerName + "> (" + schoolName + ")\n";

        // $22 ($15 off) - Paying with card
        text += "$" + total;

        if (total !== subtotal) {
            text += " ($" + (subtotal - total) + " off)";
        }

        text += " — Paying with " + paymentType + "\n";

        // David's 1st rental
        query = new Parse.Query(Transaction);
        query.equalTo("renter", renter);
        query.lessThan("createdAt", moment().subtract(1, "week").toDate());
        query.notEqualTo("renterCanceled", true);
        query.notEqualTo("ownerCanceled", true);
        query.count().then(addPrior);

        function addPrior(count) {
            text += renterFirstName + "'s ";
            count += 1;
            text += getOrdinal(count) + " rental\n";
            text += "<" + convoApi + "|" + "Read Conversation" + ">\n";
            postInSlack(text);
        }
    });
}

function postInSlack(text) {
	slack.webhook({
		channel: "#rentals",
		text: text
	}, function(err, response) {
		console.log(response);
	});
}

router.post("/sendRentalText", function(req, res){


	var transId = req.body.transactionId;

    postRentalInSlack(transId);

	console.log("rental text: " + transId);

	// Use the Twilio Cloud Module to send an SMS
	var ownerNumber = req.body.ownerNumber;
	var renterNumber = req.body.renterNumber;
	var requesterId = req.body.requesterId;
	var dressId = req.body.dressId;
	var dressName = req.body.dressName;
	var ownerName = req.body.ownerName;
	var renterName = req.body.renterName;
	var rentalDate = req.body.rentalDate;
	var ownerId = req.body.ownerId;
  
	var messageString = "👗Rental\n" + ownerName + "(" + ownerNumber + ")➡️\n" +
		renterName + "(" + renterNumber + ")\n" + dressName;

	var ownerMessage = "👗Rental\n" + renterName + " wants to rent your dress: " +
		dressName + "\nOpen Curtsy to respond";

	if (req.body.test == "false") {

		twilio.sendSms({
			From: "+16014531885",
			To: ownerNumber,
			Body: ownerMessage
		});//send to owner


	} else {
  		console.log("test mode, not sending mass texts");
	}

	return res.json("good");

});

router.post("/sendRequestText", function(req, res) {
	return(res.json("success"));
});

router.post("/cancellationText", function(req, res){
	var ownerCancel = req.body.ownerCancel;
	var ownerNumber = req.body.ownerNumber;
	var renterNumber = req.body.renterNumber;
	var dressName = req.body.dressTitle;
	var ownerName = req.body.ownerName;
	var renterName = req.body.renterName;

	var messageString = ""
	var curtsyMessage = ""
	var recipient = ""
	messageString = "Your rental for " + dressName + " has been cancelled. Message the other person for more info, or message Curtsy if you have any questions."

	if (ownerCancel == "true") {

		//send text to renter number
		curtsyMessage = "👗Cancel\n" + ownerName + "❌(+" + ownerNumber + ")➡️\n" 
						renterName + "(+" + renterNumber + ")\n" + dressName;
		messageString = ownerName + " has cancelled your rental for " +
						dressName + ". Message her for more info, or message Curtsy if you have any questions."

		recipient = renterNumber;

	} else {

		//send text to owner number
		curtsyMessage = "👗Cancel\n" + ownerName + "(+" + ownerNumber + ")➡️\n" +
						renterName + "❌(+" + renterNumber + ")\n" + dressName;
		messageString = renterName + " has cancelled her rental for your " +
						dressName + ". Message her for more info, or message Curtsy if you have any questions."

		recipient = ownerNumber;

	}

	console.log(renterNumber);
	console.log(ownerNumber);

	//send to person
	return twilio.sendSmsReq({
		From: "+16014531885",
		To: recipient,
		Body: messageString
  	}, req, res);

});

router.post("/sendReferralTexts", function(req, res){

	var numbers = req.body.numbers,
		name = req.body.name;

	console.log(req.body);

	var text = name + ": Sign up for Curtsy and get $10 off your first rental!"

	for(var i = 0; i < numbers.length; i++) {
		twilio.sendSms({
			From: "+16014531885",
			To: numbers[i],
			Body: text
		});
	}

	return res.json("success");

});

router.post("/sendCancellationText", function(req, res) {

    // Use the Twilio Cloud Module to send an SMS
	var ownerCancel = req.body.ownerCancel,
		transactionId = req.body.transactionId;

	console.log(req.body);

	var query = new Parse.Query(Parse.Object.extend("Transaction"));
	query.equalTo("objectId", transactionId);

	query.first().then(function(transaction) {
		if (ownerCancel == true){
			transaction.set("ownerCanceled", true);
			transaction.save();
		} else {
			transaction.set("renterCanceled", true);
			transaction.save();
		}
	});

  return res.json("success");

});

router.get("/test", function(req, res) {
	return res.json("hello");
});

module.exports = router;


