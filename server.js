var express = require('express'),
  fs = require('fs'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  app = express();

var ACCESS_LOG = __dirname + '/access.log';

app.set('port', 5000);
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(morgan('combined'));
app.use(morgan('combined', {
  stream: fs.createWriteStream(ACCESS_LOG, {
    flags: 'a'
  })
}));

app.use(require('./routes/twilio.js'));
app.use(require('./routes/mailgun.js'));
app.use(require('./routes/front.js'));

app.listen(app.get('port'), function() {
  console.log();
  console.log("/******************************\\");
  console.log("|    Curtsy Peripheral Server  |");
  console.log("\\******************************/")
  console.log();
  console.log(' - Port:', app.get('port'));
  console.log(' - Logging:', ACCESS_LOG);
});

module.exports = app;
