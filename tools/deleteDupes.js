var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";
var count = 0;
function queryDress(){

	var ownerName = "Maggie Raman";
	var objectId = "x3wveTbiyr";
	var dressTitle = "Urban Outfitters Romper";

	var query = new Parse.Query("Dress");
	query.equalTo("ownerName", ownerName);
	query.equalTo("title", dressTitle);
	query.notEqualTo("objectId", objectId);

	query.limit(1000);
	query.find({
		success: function(dresses){
			deleteDresses(dresses);
		}
	});
}

function deleteDresses(dresses){
	var i = 0;
	function del(){
		if(i >= dresses.length) return;
		deleteDress(dresses[i], function(){
			i++;
			del();
		});
	}
	del();
}

function deleteDress(dress, callback){
	dress.destroy({
		success: function(obj){
			count++;
			console.log("deleting " + dress.get("title") + " " + count);
			callback();
		}
	});
} 

queryDress();