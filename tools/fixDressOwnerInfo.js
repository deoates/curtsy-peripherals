var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";

function querySchools(){
    var nameQuery = new Parse.Query("Dress");
    nameQuery.doesNotExist("ownerName");
    var picQuery = new Parse.Query("Dress");
    picQuery.doesNotExist("ownerPicture");
    var query = Parse.Query.or(nameQuery, picQuery);
    query.limit(1000);
    query.descending("createdAt");
    query.find({
        success: function(dresses){
            fixDresses(dresses);
        },
        error: function(err){
            console.log(err);
        }
    });
}

function fixDresses(dresses){
    console.log(dresses.length + " dresses to fix");
    var i = 0;
    function fix(){
        if(i >= dresses.length) return;
        fixDress(dresses[i], function(){
            i++;
            fix();
        });
    }
    fix();
}

function fixDress(dress, callback){
    var owner = dress.get("owner");
    if(!owner) {
        console.log("no owner");
        return callback();
    }
    owner.fetch().then(function(){
        var image = owner.get("profilePicture");
        image.fetch().then(function(){
            var file = image.get("file");
            if(!file){
                console.log("no file");
                return callback();
            }
            dress.set("ownerName", owner.get("fullName"));
            dress.set("ownerPicture", file);
            console.log("Fixing " + dress.get("title"));
            dress.save().then(function(){
              callback();
            });
        });
    });
}

querySchools();
