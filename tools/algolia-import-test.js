var algoliasearch = require('algoliasearch');
var client = algoliasearch("SADFQP1CB0", "3842e892c6b431b357c4b5a2a8bb6db9");
var userIndex = client.initIndex('user');
var dressIndex = client.initIndex('dress');
var express = require('express');
var Parse = require('parse/node').Parse;
var router = express.Router();

Parse.initialize("M2kcrWgrrYT0tU93xAUZGI6nWCeFNSzILn8E3nhr", "fR8qNMYtsUsqqPlp7d5LMsoths7fViFsjMXCOUOL");
Parse.serverURL = "http://162.243.214.180:1338/parse";


function indexUsers() {
  var objectsToIndex = [];

  //Create a new query for User
  var query = new Parse.Query(Parse.User);
  query.descending("updatedAt");
  // Find all items
  query.find({
    success: function(users) {
      // prepare objects to index from users
      objectsToIndex = users.map(function(user) {
        // convert to regular key/value JavaScript object
        user = user.toJSON();
       // console.log(user);

        // Specify Algolia's objectID with the Parse.Object unique ID
        user.objectID = user.objectId;
        return user;
      });

      // Add or update new objects
      userIndex.saveObjects(objectsToIndex, function(err, content) {
        console.log(content);
        if (err) {
          console.log(err);
          throw err;
        }

        console.log('Parse<>Algolia import done');
      });
    },
    error: function(err) {
      console.log(err);
      throw err;
    }
  });
};

function indexDresses() {
  var objectsToIndex = [];

  //Create a new query for Dress
  var query = new Parse.Query(Parse.Object.extend("Dress"));
  query.descending("updatedAt");
  // Find all items
  query.find({
    success: function(dresses) {
      // prepare objects to index from dresses
      objectsToIndex = dresses.map(function(dress) {
        // convert to regular key/value JavaScript object
        dress = dress.toJSON();
       // console.log(user);

        // Specify Algolia's objectID with the Parse.Object unique ID
        dress.objectID = dress.objectId;
        return dress;
      });

      // Add or update new objects
      dressIndex.saveObjects(objectsToIndex, function(err, content) {
        console.log(content);
        if (err) {
          console.log(err);
          throw err;
        }

        console.log('Parse<>Algolia import done');
      });
    },
    error: function(err) {
      console.log(err);
      throw err;
    }
  });
};

indexUsers();
indexDresses();

