var ColorThief = require('color-thief'),
	colorThief = new ColorThief(),
    Parse = require('parse/node').Parse,
    express = require('express'),
    fs = require('fs'),
    request = require('request').defaults({encoding: null}),
    router = express.Router();

Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql",
  "c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse";

var sum = 0;
function queryImages(skip){
	var query = new Parse.Query("Image");
	query.limit(1);
	query.skip(skip);
	query.doesNotExist("color");
	query.descending("createdAt");
	query.find({
		success: function(images){
			if(!images){
				console.log("done");
				return;
			}
			else{
				processImages(images)
			}
		},
		error: function(err){
			console.log(err);
			return;
		}
	});
}

function processImages(images){
	console.log(sum + " - " + (sum + images.length));
	sum += images.length;
	var i = 0; 
	function process(){
		if(i >= images.length) return queryImages(sum);
		colorImage(images[i], function(){
			i++;
			process();
		});
	}
	process();
}

function colorImage(image, callback){
	console.log("coloring image " + image.id);
	image.fetch().then(function(){
		var path = image.get("file").url();
		// console.log(path);
		var file = fs.createWriteStream("file.jpg");
		function download(cb){
			request.get(path, function(error, response, body){
				response.pipe(file)
				file.on('finish', function(){
					file.close(cb);
				});
			});
		}
		download(function(){
			var im = fs.readFileSync(file.path);
			var color = colorThief.getColor(file.path);
			console.log(color);
			// color(file.path, function(err, color){
			// 	console.log('err');
			// 	if(color){
			// 		console.log("found color " + color + " for image " + image.id);
			// 		image.set("color", color);
			// 		image.save().then(function(){
			// 			callback();
			// 		});
			// 	}
			// 	else{
			// 		console.log("unable to find color");
			// 		callback();
			// 	}
			// });
		});
		// Parse.Cloud.httpRequest({ url: path}).then(function(response){
		// 	console.log("done");
		// 	console.log(response.buffer);
		
		// });
	});
}

queryImages(0);

