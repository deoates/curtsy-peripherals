var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";
var validEmails = ["curtsyapp.com","uga.edu","msstate.edu","ua.edu","auburn.edu","olemiss.edu","lsu.edu","ufl.edu","cofc.edu","clemson.edu","sc.edu","uark.edu","unc.edu","smu.edu","sewanee.edu","wlu.edu","wfu.edu","fsu.edu","usc.edu","ncsu.edu","ku.edu","uky.edu","psu.edu"];
function querySchools(){
    var query = new Parse.Query(Parse.User);
    query.doesNotExist("school");
    query.limit(1000);
    query.descending("createdAt");
    query.find({
        success: function(users){
            fixSchools(users);
        },
        error: function(err){
            console.log(err);
        }
    });
}

function fixSchools(users){
    console.log(users.length + " users to fix");
    var i = 0;
    function fix(){
        if(i >= users.length) return;
        fixUser(users[i], function(){
            i++;
            fix();
        });
    }
    fix();
}

function fixUser(user, callback){
    var email = user.get("email");
    console.log(email);
    if(!email){
        callback();
    }
    else{
        for(var i = 0; i < validEmails.length; ++i){
            if((email.indexOf('.' + validEmails[i]) >= 0) || (email.indexOf('@' + validEmails[i]) >= 0)){
                var query = new Parse.Query("School");
                query.equalTo("url", validEmails[i]);
                query.first({
                    success: function(school){
                        Parse.Cloud.run("saveUserSchool", {userId:user.id, schoolId:school.id});
                        callback();
                    },
                    error: function(err){
                        callback();
                    }
                });
            }
        }
        callback();
    }
}

querySchools();