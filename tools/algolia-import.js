var algoliasearch = require('algoliasearch');
var client = algoliasearch("G16AHHN1KC", "ddb1a20fbb711df6865a3c80d3877ef4");
var dressIndex = client.initIndex('dress');
var userIndex = client.initIndex('user');
var express = require('express');
var Parse = require('parse/node').Parse;
var router = express.Router();

Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql", "c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse";

var count = 0;

function startIndexUsers(){
  console.log("** Start Indexing Users **");
  indexUsers(0)
}
function indexUsers(skip) {
  var objectsToIndex = [];

  //Create a new query for User
  var query = new Parse.Query(Parse.User);
  query.limit(1000);
  query.skip(skip);
  query.exists("firstName");
  query.exists("lastName");
  query.exists("fullName");
  query.exists("school");

  query.descending("updatedAt");
  // Find all items
  query.find({
    success: function(users) {
      processUsers(users);
    },
    error: function(err) {
      console.log(err);
      throw err;
    }
  });
};

function processUsers(users){
  console.log("** Processing users " + count + " -> " + (users.length + count));
  count += users.length;
  if(users.length == 0) return;

   // prepare objects to index from users
  objectsToIndex = users.map(function(user) {
    // convert to regular key/value JavaScript object
    user = user.toJSON();
   // console.log(user);

    // Specify Algolia's objectID with the Parse.Object unique ID
    user.objectID = user.objectId;

    console.log("indexing user " + user.objectId);
    return user;
  });

  // Add or update new objects
  userIndex.saveObjects(objectsToIndex, function(err, content) {
    console.log(content);
    if (err) {
      console.log(err);
      throw err;
    }

    indexUsers(count);
  });
}



function indexDresses() {
  var objectsToIndex = [];

  //Create a new query for Dress
  var query = new Parse.Query(Parse.Object.extend("Dress"));
  query.notEqualTo("deleted", true);
  query.descending("updatedAt");
  query.limit(1000);
  query.skip(100);
  // Find all items
  query.find({
    success: function(dresses) {
      // prepare objects to index from dresses
      objectsToIndex = dresses.map(function(dress) {
        // convert to regular key/value JavaScript object
        dress = dress.toJSON();
       // console.log(user);

        // Specify Algolia's objectID with the Parse.Object unique ID
        dress.objectID = dress.objectId;
        return dress;
      });

      // Add or update new objects
      dressIndex.saveObjects(objectsToIndex, function(err, content) {
        console.log(content);
        if (err) {
          console.log(err);
          throw err;
        }

        console.log('Parse<>Algolia import done');
      });
    },
    error: function(err) {
      console.log(err);
      throw err;
    }
  });
};

startIndexUsers();

