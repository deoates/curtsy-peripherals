var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";
var clags = undefined;
var clagsFile = undefined;
var count = 0

function getClags(){
	console.log("---------------------------------\nStarting operation: \"Follow Agnes\"\n---------------------------------\n");
	var query = new Parse.Query(Parse.User);
	query.equalTo("objectId", "ptbHMOoEU8");
	query.first({
		success: function(user){
			clags = user;
			var pic = clags.get("profilePicture");
			pic.fetch().then(function(){
				clagsFile = pic.get("file");
				console.log("We've located Agnes\n");
				queryUsers(0);
			});
		}
	})
}

function queryUsers(skip){
	var query = new Parse.Query(Parse.User);
	query.exists("school");
	query.doesNotExist("numFollowing");
	query.exists("fullName");
	query.limit(1000);
	query.skip(skip);
	query.find({
		success: function(users){
			processUsers(users);
		}
	});
}

function processUsers(users){
	console.log("************************************");
	console.log("Processing users " + count + " - " + (users.length + count));
	console.log("************************************\n");
	count += users.length;
	if(users.length == 0) return;
	var i = 0;
	function process(){
		if(i >= users.length) return queryUsers(count);
		processUser(users[i], function(){
			i++;
			process();
		});
	}
	process();
}

function processUser(user, callback){

	console.log(user.get("fullName") + " (" + user.id + ") is now following Agnes");
	var Follow = Parse.Object.extend("Follow");
	var follow = new Follow();
	follow.set("toUser", clags);
	follow.set("fromUser", user);
	follow.set("toUserImage", clagsFile);
	var pic = user.get("profilePicture");
	if(pic){
		pic.fetch().then(function(){
			var file = pic.get("file");
			follow.set("fromUserImage", file);
			follow.save().then(function(){
				callback();
			});
		}, function(error){
			follow.save().then(function(){
				callback();
			});
		});
	}
	else{
		follow.save().then(function(){
			callback();
		});
	}
}

getClags();