var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");
Parse.serverURL = "http://162.243.214.180:1337/parse";

function queryUsers(){
  var query = new Parse.Query(Parse.User);
  query.exists("school");
  query.doesNotExist("supportConversation")
  query.limit(100);
  query.descending("createdAt");
  query.find({
    success: function(users){
      addConvoForUsers(users);
    },
    error: function(err){
      console.log(err);
    }
  });
}

function addConvoForUsers(users){
  console.log(users.length + " users to fix");
  var i = 0;
  function fix(){
    if(i >= users.length) return;
    fixUser(users[i], function(){
      i++;
      fix();
    });
  }
  fix();
}

function fixUser(user, callback){
  if(!user.get("supportConversation")){
    console.log(user.get('email'));
    Parse.Cloud.run("addSupportConvoWithUserId", {userId:user.id});
    callback();
  }
  else{
    callback();
  }  
}

queryUsers();
