var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";

var sum = 0
function queryDresses(skip){
    
    var query = new Parse.Query("Dress");
    query.limit(100);
    query.skip(skip)
    query.equalTo("loves", 0);
    query.descending("createdAt");
    query.find({
        success: function(dresses){
            countLoves(dresses);
        },
        error: function(err){
            console.log(err);
        }
    });
    
}

function countLoves(dresses){
    console.log(sum + " - " + (sum + dresses.length));
    sum += dresses.length;
    var i = 0;
    function count(){
        if(i >= dresses.length) return queryDresses(sum);
        countUser(dresses[i], function(){
            i++;
            count();
        });
    }
    count();
}

function countUser(dress, callback){
    var query = dress.relation("lovedBy").query();
    query.count({
        success: function(count) {
            console.log(dress.get("title") + ": " + count);
            dress.set("loves", count);
            dress.save().then(function(){
                callback();
            });
        },
        error: function(err){
            callback();
        }
    }); 
}

queryDresses(0);