var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";

function querySchools(){
    var query = new Parse.Query("Dress");
    query.include("owner");
    query.doesNotExist("school");
    query.limit(1000);
    query.descending("createdAt");
    query.find({
        success: function(dresses){
            fixSchools(dresses);
        },
        error: function(err){
            console.log(err);
        }
    });
}

function fixSchools(dresses){
    console.log(dresses.length + " dresses to fix");
    var i = 0;
    function fix(){
        if(i >= dresses.length) return;
        fixDress(dresses[i], function(){
            i++;
            fix();
        });
    }
    fix();
}

function fixDress(dress, callback){
    var owner = dress.get("owner");
    if(!owner) {
        console.log("no owner");
        return callback();
    }
    owner.fetch().then(function(){
        var school = owner.get("school");
        if(!school){
            console.log("no school");
            return callback();
        }
        dress.set("school", school);
        console.log("Adding " + dress.get("title") + " to " + school.get("properName"));
        dress.save().then(function(){
         callback();
    });
});
}

querySchools();
