var Parse = require('parse/node').Parse;
Parse.initialize("7eAsVaNNrFmJ7lVQU81L5J93HN2625ql","c6GY758e3y2eArK6Yyt1PISIux4jHy1E");

Parse.serverURL = "http://162.243.214.180:1337/parse";

function queryUsers(){
    
    var query = new Parse.Query(Parse.User);
    query.limit(1000);
    query.doesNotExist("numDresses");
    query.descending("createdAt");
    query.find({
        success: function(users){
            countDresses(users, function(){
                console.log("done");
            });
        },
        error: function(err){
            console.log(err);
        }
    });
    
}

function countDresses(users, callback){
    console.log(users.length + " users to count");
    var i = 0;
    function count(){
        if(i >= users.length) return;
        countUser(users[i], function(){
            i++;
            count();
        });
    }
    count();
    callback();
}

function countUser(user, callback){
    var dressQuery = user.relation("dresses").query();
    dressQuery.count({
        success: function(count) {
            console.log(user.get("fullName") + ": " + count);
            Parse.Cloud.run("countUserDresses", {userId:user.id, count:count});
            callback();
        },
        error: function(err){
            callback();
        }
    }); 
}

queryUsers();