var client = require('twilio')('ACd1df47721924949ad1114e5614fbd2f8', '86bdb94254972d4fa070f99aebeadb6a');

module.exports.sendSms = function(data) {
  client.sendMessage(data, null);
}

module.exports.sendSmsReq = function(data, req, res) {
  client.sendMessage(data, function(err, d) {
    if (err != null) {
      return res.json({
        success: false,
        message: "Uh oh, something went wrong (...guppy...)"
      });
    } else {
      return res.json({
        success: true,
        message: "Text sent!"
      });
    }
  });
}
