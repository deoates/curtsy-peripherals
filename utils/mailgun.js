var mailgun = require('mailgun-js')({
  apiKey: 'key-2c3e4a49ba6bdcb208a359ba906b4a91',
  domain: 'mg.curtsy.club'
});

module.exports.sendEmail = function(data) {
  mailgun.messages().send(data, null);
}

module.exports.sendEmailReq = function(data, req, res) {
  mailgun.messages().send(data, function(err, body) {
    if (err != null) {
      return res.json({
        success: false,
        message: "Uh oh, something went wrong (...guppy...)"
      });
    } else {
      return res.json({
        success: true,
        message: "Email sent!"
      });
    }
  });
}

module.exports.sendEmailCb = function(data, req, res, cb) {
  mailgun.messages().send(data, cb(req, res));
}

